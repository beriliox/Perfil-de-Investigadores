var request = require('request');
var cheerio = require('cheerio');
var json2csv = require('json2csv');

Router.configure({
  layoutTemplate: 'layout',
  loadingTemplate: 'loading',
  notFoundTemplate: 'notFound'
});

Router.route('/', {
  name: 'home'
});

Router.route('/inicio', {
  name: 'buscador'
});

Router.route('/about', {
  name: 'about'
});

Router.route('/results/', function () {
  this.redirect('/inicio');
});

Router.route('/download-data/:id_investigador', function(){

  var consulta = Scrapers.findOne({ num_scraper: Scrapers.find().count()});

  var fields = ['id_usuario', 'autor', 'imagen_usuario', 'institucion', 'c_campos', 'campos', 'c_titulos', 'publicaciones.titulo_pub','publicaciones.authors','publicaciones.info','publicaciones.citas','publicaciones.año', 'c_coautores', 'coautores.coautor_name',
              'coautores.a_href', 'citas_totales', 'citas_2012', 'indice_h_totales', 'indice_h_2012', 'indice_i10_totales', 'indice_i10_2012'];

  var json = consulta.datos[0];

  var result = json2csv({ data: json, fields: fields, unwindPath: ['campos', 'publicaciones', 'coautores'] });

  var headers = {
         'Content-type': 'text/csv;charset=UTF-8',
         'Content-Disposition': 'attachment; filename=' + json.id_usuario + '.csv'
  };

  this.response.writeHead(200, headers);
  this.response.write(result);
  this.response.end();
}, { where: 'server' });

Router.route('/scraper/:id_investigador', function(){
  this.response.setHeader( 'Access-Control-Allow-Origin', '*' );
  this.response.setHeader( 'Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE' );
  this.response.setHeader( 'Access-Control-Allow-Headers', 'Content-Type, X-Requested-With, x-request-metadata' );
  this.response.setHeader( "Content-Type",  "text/html; charset=utf-8" );
  this.response.setHeader( 'Access-Control-Allow-Credentials', true );


  //var array = Autores_GS.find({ sec : { $gt :  291642, $lte : 302859}}).fetch();

  //for (var iterador = 0; iterador < array.length; iterador++) {

      //var url = 'https://scholar.google.cl/citations?user=' + array[iterador].id_author_gscholar;
      var url = 'https://scholar.google.cl/citations?user=' + id_investigador;

      request(url, async function(error, response, html){

          if(!error){

          var $ = cheerio.load(html);
          //console.log(html);

          var id_usuario, autor, imagen_usuario, institucion, c_campos, campos, publicaciones, c_titulos, citas_totales, citas_2012, indice_h_totales, indice_h_2012,
              indice_i10_totales, indice_i10_2012, titulo_pub, authors, info, citas, año;

          var separador_campos = [], campos_array = [], markers = [], coautores_array = [], datos_array = [];

          var json_pre = {  id_usuario: "",
                            autor : "",
                            imagen_usuario: "",
                            institucion : "",
                            c_campos : "",
                            campos : campos_array,
                            c_titulos : "",
                            publicaciones : markers,
                            c_coautores : "",
                            coautores : coautores_array,
                            citas_totales : "",
                            citas_2012 : "",
                            indice_h_totales : "",
                            indice_h_2012 : "",
                            indice_i10_totales : "",
                            indice_i10_2012 : ""
                         };

          var json_pre_2 = {  id_usuario: "",
                              autor : "",
                              imagen_usuario: "",
                              institucion : "",
                              c_campos : "",
                              campos : campos_array,
                              c_titulos : "",
                              publicaciones : markers,
                              c_coautores : "",
                              coautores : coautores_array,
                              citas_totales : "",
                              citas_2012 : "",
                              indice_h_totales : "",
                              indice_h_2012 : "",
                              indice_i10_totales : "",
                              indice_i10_2012 : "",
                              datos : datos_array
                           };

          $('#gsc_prf_in').filter(function(){ //LISTO
            var data = $(this);
            autor = data.text().trim();

            json_pre.autor = autor;
            json_pre_2.autor = autor;

            //console.log('autor: ' + autor);
          })

          $('#gsc_md_cbyd_f').filter(function(){ //LISTO
            var data = $(this);

            var cut = data.attr('action');

            var cut1 = cut.split("user=");
            //console.log('autor: ' + autor);

            var cut2 = cut1[1].split("&");

            id_usuario = cut2[0];
            json_pre.id_usuario = id_usuario.toString();
            json_pre_2.id_usuario = id_usuario.toString();

            //console.log("id_usuario: " + id_usuario);
          })

          $('#gsc_prf_pup-img').filter(function(){ //LISTO
            var data = $(this);
            imagen_usuario = data.attr('src');

            json_pre.imagen_usuario = imagen_usuario;
            json_pre_2.imagen_usuario = imagen_usuario;

            //console.log("imagen usuario: " + imagen_usuario);
          })


          $('#gsc_prf_i').filter(function(){ //LISTO
            var data = $(this);
            c_campos = data.children().eq(3).children().length;

            json_pre.c_campos = c_campos;
            json_pre_2.c_campos = c_campos;

            //console.log("c_campos: " + c_campos);
          })

          $('#gsc_prf_i').filter(function(){ //LISTO
            var data = $(this);
            hijos = data.children().eq(3).children(); // .eq(0).text().trim()
            campos = "";

            for (var i = 0; i <= c_campos - 1; i++) {
              campos += hijos.eq(i).text().trim().concat(",");
            }
            separador_campos = campos.split(',');
            campos_array = separador_campos.slice(0, c_campos);
            json_pre.campos = campos_array;
            json_pre_2.campos = campos_array;


            //console.log("campos: " + json_pre.campos);
          })

          $('#gsc_prf_i').filter(function(){ //LISTO
            var data = $(this);
            institucion = data.children().eq(1).children().eq(0).text().trim();

            //institucion = data.next().children().attr('href');


            if (institucion == "") {
              json_pre.institucion = 'Sin Institución Registrada';
              json_pre_2.institucion = 'Sin Institución Registrada';

            } else {
              json_pre.institucion = institucion;
              json_pre_2.institucion = institucion;

            }

            //console.log("institucion: " + json_pre.institucion);
          })

          $('#gsc_a_b').filter(function(){ //LISTO
            var data = $(this);
            c_titulos = data.children().length;

            json_pre.c_titulos = c_titulos;
            json_pre_2.c_titulos = c_titulos;


            //console.log("c_titulos: " + c_titulos);
          })


          $('#gsc_a_b').filter(function(){ //LISTO
            var data = $(this);
            hijos = data.children(); // .eq(0).text().trim()

            for (var i = 0; i <= c_titulos - 1; i++) {

              markers[i] = {  titulo_pub: "",
                              authors: "",
                              info: "",
                              citas : "",
                              año: ""
                           };

              titulo_pub = hijos.eq(i).children().eq(0).children().eq(0).text().trim(); //console.log("titulo:" + titulo_pub);
              authors = hijos.eq(i).children().eq(0).children().eq(1).text().trim(); //console.log("author:" + authors);
              info = hijos.eq(i).children().eq(0).children().eq(2).text().trim(); //console.log("info:" + info);
              citas = hijos.eq(i).children().eq(1).children().eq(0).text().trim(); //console.log("citas: " + citas);
              año = hijos.eq(i).children().eq(2).children().eq(0).text().trim(); //console.log("año: " + año);

              if (titulo_pub == "") {
                markers[i].titulo_pub = "Sin título registrado";
              } else {
                markers[i].titulo_pub = titulo_pub;
              }

              if (authors == "") {
                markers[i].authors = "Sin autores registrados";
              } else {
                markers[i].authors = authors;
              }



              if (info == "") {
                markers[i].info = "Sin revista registrada";
              } else {
                var info_f = info.split(',');
                var info_final_1 = info_f[0];
                var info_1 = info_final_1.split('(');
                var info_final = info_1[0];

                var info_final_f = info_final.match(/\D/ig);

                var info_s = info_final_f.join(separador = '').trim().toLowerCase();
                markers[i].info = info_s;
              }



              if (citas == "") {
                markers[i].citas = "0";
              } else {
                markers[i].citas = citas;
              }

              if (año == "") {
                markers[i].año = "Año no registrado";
              } else {
                markers[i].año = año;
              }

              markers.fill(markers[i], i, i + 1);
            }

          })

          $('#gsc_rsb_co').filter(function(){ //LISTO
            var data = $(this);
            c_coautores = data.children().eq(1).children().length;

            json_pre.c_coautores = c_coautores -1;
            json_pre_2.c_coautores = c_coautores -1;

            //console.log("c_coautores: " + json_pre.c_coautores);
          })


          $('#gsc_rsb_co').filter(function(){ //LISTO
            var data = $(this);
            hijos = data.children().eq(1).children(); // .eq(0).text().trim()

            for (var i = 0; i <= c_coautores - 2; i++) {

              coautores_array[i] = {  autor_original : "",
                                      coautor_name: "",
                                      a_href: "",
                                      id_coautor: ""
                                   };

              coautor_name = hijos.eq(i).children().eq(0).children().eq(2).children().eq(0).text().trim();
              a_href = hijos.eq(i).children().eq(0).children().eq(2).children().eq(0).attr('href');

              ff = a_href.split("=");
              ee = ff[1];
              dd = ee.split("&");
              id_coautor = dd[0];

              coautores_array[i].autor_original = autor;
              coautores_array[i].coautor_name = coautor_name;
              coautores_array[i].a_href = 'https://scholar.google.cl'.concat(a_href);
              coautores_array[i].id_coautor = id_coautor;

              coautores_array.fill(coautores_array[i], i, i +1);
            }

            //console.log("coautores: " + json_pre.coautores);

          })

          //CITAS
          $('#gsc_rsb_st').filter(function(){ //LISTO
            var data = $(this);
            citas_totales = data.children().eq(1).children().eq(0).children().eq(1).text();

            json_pre.citas_totales = citas_totales;
            json_pre_2.citas_totales = citas_totales;


            //console.log("citas_totales: " + json_pre.citas_totales);
          })

          $('#gsc_rsb_st').filter(function(){ //LISTO
            var data = $(this);
            citas_2012 = data.children().eq(1).children().eq(0).children().eq(2).text();

            json_pre.citas_2012 = citas_2012;
            json_pre_2.citas_2012 = citas_2012;

            //console.log("citas_2012: " + json_pre.citas_2012);
          })

          $('#gsc_rsb_st').filter(function(){ //LISTO
            var data = $(this);
            indice_h_totales = data.children().eq(1).children().eq(1).children().eq(1).text();

            json_pre.indice_h_totales = indice_h_totales;
            json_pre_2.indice_h_totales = indice_h_totales;

            //console.log("indice_h_totales: " + json_pre.indice_h_totales);
          })

          $('#gsc_rsb_st').filter(function(){ //LISTO
            var data = $(this);
            indice_h_2012 = data.children().eq(1).children().eq(1).children().eq(2).text();

            json_pre.indice_h_2012 = indice_h_2012;
            json_pre_2.indice_h_2012 = indice_h_2012;

            //console.log("indice_h_2012: " + json_pre.indice_h_2012);
          })

          $('#gsc_rsb_st').filter(function(){ //LISTO
            var data = $(this);
            indice_i10_totales = data.children().eq(1).children().eq(2).children().eq(1).text();

            json_pre.indice_i10_totales = indice_i10_totales;
            json_pre_2.indice_i10_totales = indice_i10_totales;

            //console.log("indice_i10_totales: " + json_pre.indice_i10_totales);
          })

          $('#gsc_rsb_st').filter(function(){ //LISTO
            var data = $(this);
            indice_i10_2012 = data.children().eq(1).children().eq(2).children().eq(2).text();

            json_pre.indice_i10_2012 = indice_i10_2012;
            json_pre_2.indice_i10_2012 = indice_i10_2012;

          })


        datos_array[0] = json_pre;
        datos_array.fill(datos_array[0]);

        var json_final = _.extend(json_pre_2, {
          num_scraper: Scrapers.find().count()+1,
          //num_scraper: iterador ++,
        });

        //console.log(json_final);
          var id_usuario_m = Scrapers.findOne({id_usuario :json_final.id_usuario });
          if (id_usuario_m) {
           Scrapers.update({id_usuario :json_final.id_usuario }, {$set: json_final});
            console.log('Usuario Actualizado');
            //console.log(json_final);
          } else {
            Scrapers.insert(json_final);
            console.log('Usuario Insertado')
            //console.log(json_final);
          }

      } else {
        console.log('Error en la consulta');
      }
    })

    var busq = Busquedas.findOne({id_investigador : Busquedas.findOne({sec : Busquedas.find().count()}).id_investigador});

    this.response.writeHead(302, {"Location": "/results/" + busq.id_investigador});

    this.response.end();

}, {where : "server"});

Router.route('/results/:id_investigador', {
   name: 'results',
   waitOn: function() {
     return Meteor.subscribe('busquedas', this.params.id_investigador);
   },
   data: function() {
     return Busquedas.findOne(this.params.id_investigador);
   }

});

var requireLogin = function() {
  if (! Meteor.user()) {
    if (Meteor.loggingIn()) {
      this.render(this.loadingTemplate);
    } else {
      this.render('accessDenied');
    }
  } else {
    this.next();
  }
}
