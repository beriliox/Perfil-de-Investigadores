Busquedas = new Mongo.Collection('busquedas');

Meteor.methods({
  BusquedaInsert: function(busquedaAttributes) {
    check(this.userId, String);
    check(busquedaAttributes, {
      id_investigador: String
    });

    var user = Meteor.user();

    var busqueda = _.extend(busquedaAttributes, {
      sec: Busquedas.find().count()+1,
      userId: user._id,
      author: user.username,
      email: user.emails,
      submitted: new Date()
    });


    var id_busqueda = Busquedas.insert(busqueda);

    id_investigador = busquedaAttributes.id_investigador;


    return {
      id_investigador: id_investigador
    };
  },
  ExportarJSON: function(busquedaAttributes) {
    check(this.userId, String);
    check(busquedaAttributes, {
      id_investigador: String
    });

    id_investigador = busquedaAttributes.id_investigador;


    return {
      id_investigador: id_investigador
    };
  }

});
