var request = require('request');
var cheerio = require('cheerio');

Template.results.helpers({
  scraper : function() {
    var url = window.location.pathname;
    var res = url.split("/");
    var id_usuario_s = res[2];
    var scrap = Scrapers.find({id_usuario : id_usuario_s});
    return scrap;
  }
})

Template.perfil.inheritsHelpersFrom('results');
Template.colaboradores.inheritsHelpersFrom('results');
Template.tree.inheritsHelpersFrom('results');
Template.barras.inheritsHelpersFrom('results');
//Template.redes.inheritsHelpersFrom('results');
