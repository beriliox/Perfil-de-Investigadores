//Session.setDefault("skip", 0);

var url = window.location.pathname;
var res = url.split("/");
var id_usuario_p = res[2];

/*Deps.autorun(function() {
  Meteor.subscribe("scrapers", Session.get("skip"))
});*/

Template.colaboradores.helpers({
  'colaboradores': function() {
    return Scrapers.find({id_usuario: id_usuario_p});
  }
});

Template.colaboradores.events({
  'click .coautor_path': function(event, template) {

    var id_usuario = event.target.getAttribute("id");
    //console.log(id_usuario);

    var busqueda = {
      id_investigador: id_usuario
    };

    Meteor.call('BusquedaInsert', busqueda, function(error, result) {

      Router.go('/scraper/' + result.id_investigador, {id_investigador: result.id_investigador});
    });

  }
  /*,
  'click .previous': function() {
    Session.set('skip', Session.get('skip')-2);
  },
  'click .next': function() {
    Session.set('skip', Session.get('skip')+2);
  }*/
});
