//import {Excel} from 'meteor/nicolaslopezj:excel-export'

Template.tree.onCreated(function(){
  this.showPub = new ReactiveVar( false );
  this.showRev = new ReactiveVar( false );
  this.showList = new ReactiveVar( false );
});

Template.tree.helpers({
  showList: function() {
    return Template.instance().showList.get();
  },
  showRev: function() {
    return Template.instance().showRev.get();
  }
});
Template.tree.events({
  "click .exportar":function(){

    var url_2 = window.location.pathname;
    var res_2 = url_2.split("/");
    var id_usuario_s_2 = res_2[2];
    var id_investigador = res_2[2];

    //var csvContent = CSV.unparse(Scrapers.find({id_usuario: id_usuario_s_2}).fetch());
    //window.open('data:text/csv;charset=utf-8,' + escape(csvContent), '_self');

    var exportar = {
      id_investigador: id_investigador
    };

    //Router.go('/download-data/' + id_inv, {id_inv: id_inv});

    Meteor.call('ExportarJSON', exportar, function(error, result) {
      if (error)
        return throwError(error.reason);

      Router.go('/download-data/' + result.id_investigador, {id_investigador: result.id_investigador});
    });


  },

  'change form': function( event, template ) {
    if ( $( event.target ).val() === "showlist" ) {
      template.showList.set( true );
      template.showRev.set( false );
      template.showPub.set( false );

    } else if ( $( event.target ).val() === "showrev" ) {
      template.showRev.set( true );
      template.showPub.set( false );
      template.showList.set( false );

    } else {
      template.showPub.set( true );
      template.showRev.set( false );
      template.showList.set( false );

    }
  }
});
