Template.buscador.events({

});

Template.buscador.onCreated(function() {
  Session.set('BusquedaError', {});
});

Template.buscador.helpers({
  errorMessage: function(field) {
    return Session.get('BusquedaError')[field];
  },
  errorClass: function (field) {
    return !!Session.get('BusquedaError')[field] ? 'has-error' : '';
  }
});
