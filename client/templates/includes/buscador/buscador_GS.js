Template.buscador_GS.onCreated(function(){
  this.showURL = new ReactiveVar( false );
  this.showName = new ReactiveVar( false );
});

Template.buscador_GS.helpers({
  showURL: function() {
    return Template.instance().showURL.get();
  },
  settings: function() {
    return {
      position: "down",
      limit: 9,
      rules: [
        {
          collection: 'Autores_GS',
          field: "name",
          template: Template.userPill
        }
      ]
    };
  }
});

Template.buscador_GS.events({
  'change form': function( event, template ) {
    if ( $( event.target ).val() === "showurl" ) {
      template.showURL.set( true );
    } else {
      template.showURL.set( false);
    }
  },
  "autocompleteselect input": function(event, template, doc) {
    id_author_gscholar = doc.id_author_gscholar;
    console.log("selected ", id_author_gscholar);

    event.preventDefault();

    var busqueda = {
      id_investigador: id_author_gscholar
    };

    Meteor.call('BusquedaInsert', busqueda, function(error, result) {

      Router.go('/scraper/' + result.id_investigador, {id_investigador: result.id_investigador});
    });

  },
  "submit .new_search": function(event, template) {

    event.preventDefault();

    const target = event.target;
    const text = target.id_investigador.value;

    var corte_1 = text.split("=");
    var id_u = corte_1[1]

    var corte_2 = id_u.split("&");
    var id_author_gscholar = corte_2[0];
    console.log(id_author_gscholar);

    var busqueda = {
      id_investigador: id_author_gscholar
    };


    Meteor.call('BusquedaInsert', busqueda, function(error, result) {
      
      Router.go('/scraper/' + result.id_investigador, {id_investigador: result.id_investigador});
    });
  }
});
