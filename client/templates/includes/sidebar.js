Template.sidebar.events({
  'submit form': function(e) {
    e.preventDefault();

    var busqueda = {
      id_investigador: $(e.target).find('[name="id_investigador_2"]').val()
    };

    var errors = validateBusqueda(busqueda);

    if (errors.id_investigador)
      return Session.set('BusquedaError', errors);

    Meteor.call('BusquedaInsert', busqueda, function(error, result) {
      if (error)
        return throwError(error.reason);

      console.log(result.id_investigador)
      console.log("========================")
      Router.go('results', {id_investigador: result.id_investigador});
    });
  }
});

Template.sidebar.onCreated(function() {
  Session.set('BusquedaError', {});
});

Template.sidebar.helpers({
  errorMessage: function(field) {
    return Session.get('BusquedaError')[field];
  },
  errorClass: function (field) {
    return !!Session.get('BusquedaError')[field] ? 'has-error' : '';
  }
});
