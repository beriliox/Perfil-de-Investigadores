module.exports = {
  servers: {
    one: {
      host: '165.227.195.95',
      username: 'root',
      pem: '~/.ssh/id_carlo_digital'

    }
  },

  app: {
    name: 'perfilesgs',
    path: '.',

    servers: {
      one: {},
    },

    buildOptions: {
      serverOnly: true,
    },

    env: {
      ROOT_URL: 'http://perfilesgs.tk',
      MONGO_URL: 'mongodb://xevedark:Pizzicato5@ds255715.mlab.com:55715/perfilesgs',
    },

    docker: {
      image: 'abernix/meteord:base',
      prepareBundle: false
    },

    enableUploadProgressBar: true
  }
};
